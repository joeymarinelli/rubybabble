require_relative "tile_group.rb"
require_relative "tile_bag.rb"

#This is the Word class. It inherits from the TileGroup class
class Word < TileGroup
	#This is the constructor for the Word class. It will call the constructor from the TileGroup class
	def initialize
		super	
	end 

	#This is the score method. It will calculate and return the sum of the point values for the word
	def score
		score = 0		
		tiles.each {|tile| 
				score += TileBag.points_for tile
			}
		score
	end
end
