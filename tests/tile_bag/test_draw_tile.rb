require "minitest/autorun"
require_relative "../../tile_bag.rb"

#This is the test class for the draw tile method
class TestDrawTile < Minitest::Test
	#this method will set up the TileBag object 
	def setup
		@tb = TileBag.new
	end

	##this is the test method that confirms the tilebag has the correct number of tiles and that when all of them are drawn
	#the bag is empty

	def test_has_proper_number_of_tiles
		assert_equal 98, @tb.tile_bag.length
		@tb.draw_tile
		assert_equal false, @tb.empty?
		assert_equal 97, @tb.tile_bag.length
		97.times {@tb.draw_tile}
		assert_equal 0, @tb.tile_bag.length
		assert_equal true, @tb.empty?
	end

	#this is the test method that will confirm that the tiles have the correct distribution
	def test_has_proper_tile_distribution
		assert_equal 9, check_distribution(:A)
		assert_equal 2, check_distribution(:B)
		assert_equal 2, check_distribution(:C)
		assert_equal 4, check_distribution(:D)
		assert_equal 12, check_distribution(:E)
		assert_equal 2, check_distribution(:F)
		assert_equal 3, check_distribution(:G)
		assert_equal 2, check_distribution(:H)
		assert_equal 9, check_distribution(:I)
		assert_equal 1, check_distribution(:J)
		assert_equal 1, check_distribution(:K)
		assert_equal 4, check_distribution(:L)
		assert_equal 2, check_distribution(:M)
		assert_equal 6, check_distribution(:N)
		assert_equal 8, check_distribution(:O)
		assert_equal 2, check_distribution(:P)
		assert_equal 1, check_distribution(:Q)
		assert_equal 6, check_distribution(:R)
		assert_equal 4, check_distribution(:S)
		assert_equal 6, check_distribution(:T)
		assert_equal 4, check_distribution(:U)
		assert_equal 2, check_distribution(:V)
		assert_equal 2, check_distribution(:W)
		assert_equal 1, check_distribution(:X)
		assert_equal 2, check_distribution(:Y)
		assert_equal 1, check_distribution(:Z)
	end

	private

	#this is the helper method that will check the number of tiles that have each letter. 
	def check_distribution letter
		count = 0
		@tb.tile_bag.each {|tile| 
				if tile == letter
					count += 1	
				end
				}
		return count
	end
		
end
