require "minitest/autorun"
require_relative "../../tile_bag.rb"

#This is the test class for the points_for method. It will use tests to confirm that thetiles have the correct point values
class TestPointsFor < Minitest::Test
	#This is the setup method that will create the TileBag object
	def setup
		@tb = TileBag.new
	end

	#This test will confirm that the point values are correct per the Scrabble guidelines
	def test_confirm_point_values
		assert_equal true, points_for_is_value?
		assert_equal [:A, :E, :I, :L, :N, :O, :R, :S, :T, :U], one_point_values
		assert_equal [:D, :G], two_point_values	
		assert_equal [:B, :C, :M, :P], three_point_values
		assert_equal [:F, :H, :V, :W, :Y], four_point_values
		assert_equal [:K] , five_point_values
		assert_equal [:J, :X], eight_point_values
		assert_equal [:Q, :Z], ten_point_values

	end

	private
	#this is the helper method that will check that the point_for class method will return the value of the hash
	def points_for_is_value? 
		points_is_value = true
		TileBag.points.each {|key, value| 
				if TileBag.points_for(key) != value
					points_is_value = false
				end
		}
		return points_is_value
	end

	#this helper method creates an array of all the tiles that have a point value of 1
	def one_point_values 
		point_array_1 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 1
					point_array_1 << key
				end
		}
		return point_array_1
	end

	#this helper method creates an array of all the tiles that have a point value of 2
	def two_point_values
		point_array_2 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 2
					point_array_2 << key
				end
		}
		return point_array_2
	end

	#this helper method creates an array of all the tiles that have a point value of 3
	def three_point_values 
		point_array_3 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 3
					point_array_3 << key
				end
		}
		return point_array_3
	end

	#this helper method creates an array of all the tiles that have a point value of 4
	def four_point_values
		point_array_4 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 4
					point_array_4 << key
				end
		}
		return point_array_4
	end

	#this helper method creates an array of all the tiles that have a point value of 5
	def five_point_values
		point_array_5 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 5
					point_array_5 << key
				end
		}
		return point_array_5
	end

	#this helper method creates an array of all the tiles that have a point value of 8
	def eight_point_values
		point_array_8 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 8
					point_array_8 << key
				end
		}
		return point_array_8
	end

	#this helper method creates an array of all the tiles that have a point value of 10
	def ten_point_values
		point_array_10 = []
		
		TileBag.points.each {|key, value|
				if TileBag.points_for(key) == 10
					point_array_10 << key
				end
		}
		return point_array_10
	end

end
