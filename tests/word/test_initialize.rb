require "minitest/autorun"
require_relative "../../word.rb"
#this is the test class for the Word initialize method
class Word::TestInitialize < Minitest::Test
	#This will set up a new Word object for the test
	def setup 
		@wd = Word.new		
	end

	#This will test that an empty word is created
	def test_create_empty_word
		assert_equal 0, @wd.tiles.length
		assert_equal Array, @wd.tiles.class
	end
end
