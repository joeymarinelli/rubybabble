require "minitest/autorun"
require_relative "../../word.rb"

#this is the test class for the Word initialize method
class TestScore < Minitest::Test
	#This will set up a new Word object for the test
	def setup 
		@wd = Word.new	
	end

	#This will test that an empty word has a score of zero
	def test_empty_word_should_have_score_of_zero
		assert_equal 0, @wd.score
		assert_equal 0, @wd.tiles.length
	end
	
	#This will test that a one tile word can be scored
	def test_score_a_one_tile_word
		@wd.append(:I)
		assert_equal 1, @wd.score
	end

	#This will test that a word with multiple different tiles is scored correctly
	def test_score_a_word_with_multiple_different_tiles
		@wd.append(:I)
		@wd.append(:C)
		@wd.append(:E)
		assert_equal 5, @wd.score
	end

	#This will test that a word with recurring tiles is scored correctly
	def test_score_a_word_with_recurring_tiles
		@wd.append(:A)
		@wd.append(:P)
		@wd.append(:P)
		@wd.append(:L)
		@wd.append(:E)
		
		assert_equal 9, @wd.score
	end
	
end
