require "minitest/autorun"
require_relative "../../tile_group.rb"
#this is the test class for the TileGroup hand method
class TileGroup::TestHand < Minitest::Test
	#This will set up a new TileGroup object for the test
	def setup 
		@tg = TileGroup.new
	end

	#This will test that an empty tile group is converted to a string
	def test_convert_empty_group_to_string
		assert_equal @tg.hand, ""
	end

	#This will test that a tile group with a single tile is converted to a string
	def test_convert_single_tile_group_to_string
		@tg.append(:A)
		assert_equal "A", @tg.hand
	end
	
	#This will test that a tile group with multiple tiles is converted to a string
	def test_convert_multi_tile_group_to_string
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		assert_equal "ABC", @tg.hand
	end	

	#This will test that a tile group with duplicate tiles is converted to a string
	def test_convert_multi_tile_group_with_duplicates_to_string
		@tg.append(:A)
		@tg.append(:A)
		@tg.append(:C)
		assert_equal "AAC", @tg.hand
	end
end
