require "minitest/autorun"
require_relative "../../tile_group.rb"
#this is the test class for the TileGroup initialize method
class TileGroup::TestInitialize < Minitest::Test
	#This will set up a new TileGroup object for the test
	def setup 
		@tg = TileGroup.new
	end

	#This will test that an empty tile group is created
	def test_create_empty_tile_group
		assert_equal 0, @tg.tiles.length
		assert_equal Array, @tg.tiles.class
	end
end
