require "minitest/autorun"
require_relative "../../tile_group.rb"

#This is the test class for the append method in the TileGroup class
class TestAppend < Minitest::Test

	#This will set up a new TileGroup object for each test
	def setup
		@tg = TileGroup.new
	end

	#This will test the one tile can be appended to the group
	def test_append_one_tile
		@tg.append(:A)
		assert_equal [:A], @tg.tiles
		
	end

	#This will test the many tiles can be appended to a group
	def test_append_many_tiles
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		@tg.append(:D)
		@tg.append(:E)
		@tg.append(:F)
		@tg.append(:G)
		@tg.append(:H)
		assert_equal [:A, :B, :C, :D, :E, :F, :G, :H], @tg.tiles
	end

	#This will test that duplicate tiles can be appended to a group
	def test_append_duplicate_tiles
		@tg.append(:A)
		assert_equal [:A, :A], @tg.append(:A)
		7.times {@tg.append(:A)}
		assert_equal [:A, :A, :A, :A, :A, :A, :A, :A, :A], @tg.tiles
	end
end
