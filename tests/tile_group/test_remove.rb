require "minitest/autorun"
require_relative "../../tile_group.rb"
#This is the test class for the remove method in the TileGroup class
class TestRemove < Minitest::Test
	#This will set up the tests with a new TileGroup object
	def setup 
		@tg = TileGroup.new
	end
	
	#This will test that a tile can be removed from a TileGroup that contains only one tile
	def test_remove_only_tile
		@tg.append(:A)
		assert_equal 1, @tg.tiles.length
		assert_equal [:A], @tg.tiles
		@tg.remove(:A)
		assert_equal 0, @tg.tiles.length
		assert_equal [], @tg.tiles
	end
	
	#This will test that the first tile in a group can be removed
	def test_remove_first_tile_from_many
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		@tg.append(:D)
		@tg.append(:E)
		tile1 = @tg.tiles[0]
		@tg.remove(tile1)
		assert_equal [:B, :C, :D, :E], @tg.tiles
		
	end

	#This will test that the last tile in a group can be removed
	def test_remove_last_tile_from_many
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		@tg.append(:D)
		@tg.append(:E)
		tile1 = @tg.tiles[@tg.tiles.length - 1]
		@tg.remove(tile1)
		assert_equal [:A, :B, :C, :D], @tg.tiles
	end

	#This will test that the middle tile of a group can be removed
	def test_remove_middle_tile_from_many
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		@tg.append(:D)
		@tg.append(:E)
		@tg.append(:F)
		@tg.append(:G)
		tile1 = @tg.tiles[(@tg.tiles.length - 1) / 2]
		@tg.remove(tile1)
		assert_equal [:A, :B, :C, :E, :F, :G], @tg.tiles
	end

	#This will test that multiple tiles can be removed from a group
	def test_remove_multiple_tiles
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		@tg.append(:D)
		@tg.append(:E)
		@tg.append(:F)
		@tg.append(:G)
		@tg.remove(:A)
		@tg.remove(:B)
		@tg.remove(:C)
		assert_equal [:D, :E, :F, :G], @tg.tiles
	end

	#This will test that if there are duplicate tiles in a group, only one of them will be removed
	def test_make_sure_duplicates_are_not_removed
		@tg.append(:A)
		@tg.append(:A)
		@tg.append(:A)
		@tg.append(:A)
		@tg.append(:A)
		assert_equal [:A, :A, :A, :A, :A], @tg.tiles
		@tg.remove(:A)
		assert_equal [:A, :A, :A, :A], @tg.tiles
		@tg.remove(:A)
		assert_equal [:A, :A, :A], @tg.tiles
	end
end
