require "minitest/autorun"
require_relative "../../tile_rack.rb"

#This is the test class for the has_tiles_for? method of the TileRack class. 
class TileRack::TestHasTilesFor < Minitest::Test
	#this will set up each test method with a new TileRack object
	def setup
		@tr = TileRack.new
	end

	#This test will confirm that the method will return true when the tiles are in order
	#on the rack and have no duplicate letters
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		@tr.append(:T)
		@tr.append(:I)
		@tr.append(:L)
		@tr.append(:E)
		
		assert_equal true, @tr.has_tiles_for?("TILE")
	end

	#This test will confirm that the method works properly when the letters are not in 
	#order on the rack
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		@tr.append(:I)
		@tr.append(:T)
		@tr.append(:E)
		@tr.append(:L)
		
		assert_equal true, @tr.has_tiles_for?("TILE")
	end

	#This test will confirm that the method returns false when the rack does not contain
	#any of the letters needed for the word
	def test_rack_doesnt_contain_any_needed_letters	
		@tr.append(:A)
		@tr.append(:B)
		@tr.append(:C)
		@tr.append(:D)
		
		assert_equal false, @tr.has_tiles_for?("TILE")		
	end

	#This test will confirm that the method returns false when the rack only contains some 
	#of the letters needed for the word
	def test_rack_contains_some_but_not_all_needed_letters
		@tr.append(:T)
		@tr.append(:I)
		@tr.append(:C)
		@tr.append(:D)
		
		assert_equal false, @tr.has_tiles_for?("TILE")	
	end 

	#This test will confirm that the method works properly for words that contain duplicate letters
	def test_rack_contains_a_word_with_duplicate_letters
		@tr.append(:F)
		@tr.append(:A)
		@tr.append(:L)
		@tr.append(:L)
		
		assert_equal true, @tr.has_tiles_for?("Fall")
	end

	#This test will confirm that the test returns false if the rack does not contain enough duplicate letters
	def test_rack_doesnt_contain_enough_duplicate_letters
		@tr.append(:L)
		@tr.append(:L)
		@tr.append(:L)
		@tr.append(:L)
		
		assert_equal false, @tr.has_tiles_for?("lllll")
	end
end
