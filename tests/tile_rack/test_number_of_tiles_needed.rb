require "minitest/autorun"
require_relative "../../tile_rack.rb"

#This is the test class fdor the number_of_tiles_needed method in the TileRack class
class TileRack::TestNumberOfTilesNeeded < Minitest::Test
	#This method will set up a new TileRack object for each of the test methods. 
	def setup
		@tr = TileRack.new
	end

	#This test will confirm that when a tile rack is empty, it will need the maximum number of tiles
	def test_empty_tile_rack_should_need_max_tiles
		assert_equal TileRack.class_variable_get(:@@MAX_TILES), @tr.number_of_tiles_needed
	end

	#This test will confirm that when a tile rack has one tile, it will need the maximum number 
	#of tiles minus 1
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		@tr.append(:A)
		assert_equal TileRack.class_variable_get(:@@MAX_TILES) - 1, @tr.number_of_tiles_needed
	end

	#This test will confirm that the correct number of tiles is returned when the rack has multiple tiles
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		@tr.append(:A)
		@tr.append(:B)
		@tr.append(:C)
		assert_equal TileRack.class_variable_get(:@@MAX_TILES) - 3, @tr.number_of_tiles_needed
	end

	#This test will confirm that when the rack has the maximum number of tiles, the method will return 0
	def test_that_full_tile_rack_doesnt_need_any_tiles
	 	@tr.append(:A)
		@tr.append(:B)
		@tr.append(:C)
		@tr.append(:A)
		@tr.append(:B)
		@tr.append(:C)
		@tr.append(:A)
		assert_equal 0, @tr.number_of_tiles_needed
	end
end
