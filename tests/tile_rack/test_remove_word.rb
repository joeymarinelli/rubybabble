require "minitest/autorun"
require_relative "../../tile_rack.rb"

#This is the test class for the remove_word method of the TileRack class
class TileRack::TestRemoveWord < Minitest::Test
	
	#This will set up a new TileRack object for each test method
	def setup 
		@tr = TileRack.new
	end
	
	#This test will confirm that the method will remove a word whose letters are in order on the rack
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		@tr.append(:T)
		@tr.append(:I)
		@tr.append(:L)
		@tr.append(:E)
		assert_equal [:T, :I, :L, :E], @tr.remove_word("tile").tiles
	end

	#This test will confirm that the method will remove a word whose letters are not in order on the rack
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		@tr.append(:L)
		@tr.append(:E)
		@tr.append(:T)
		@tr.append(:I)

		assert_equal [:T, :I, :L, :E], @tr.remove_word("tile").tiles
	end

	#This test will confirm the method will remove a word with duplicate letters
	def test_can_remove_word_with_duplicate_letters 
		@tr.append(:A)
		@tr.append(:P)
		@tr.append(:P)
		@tr.append(:L)
		@tr.append(:E)

		assert_equal [:A, :P, :P, :L, :E], @tr.remove_word("apple").tiles
	end

	#This test will confirm that unneeded duplicate letters are not removed from the rack
	def test_can_remove_word_without_removing_unneeded_duplicate_letters
		@tr.append(:A)
		@tr.append(:P)
		@tr.append(:P)
		@tr.append(:L)
		@tr.append(:E)
		@tr.append(:P)
		@tr.append(:P)
		assert_equal [:A, :P, :P, :L, :E, :P, :P], @tr.tiles
		@tr.remove_word("apple").tiles
		assert_equal [:P, :P], @tr.tiles
	end
end
