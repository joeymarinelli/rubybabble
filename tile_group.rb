class TileGroup
	
	attr_accessor :tiles
	
	#this is the constructor of the TileGroup class. It will initialize an empty array of tiles
	def initialize
		@tiles = []
	end
	
	#this is the append method that will add a tile to the array of tiles
	def append tile
		if tile.class != Symbol
			"Tile must be a symbol"
		else
			tiles << tile
		end
	end

	#this is the remove method that will remove a tile from the array of tiles
	def remove tile
		if tile.class != Symbol
			"Tile must be a symbol"
		else
			index_of_tile = tiles.index(tile)
			tiles.delete_at(index_of_tile)
		end
	end

	#this is the hand method that will return the tiles in the array as a string
	def hand
		tiles_as_string = ""
		tiles.each {|tile| tiles_as_string += tile.to_s}
		return tiles_as_string
	end
end
