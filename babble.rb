require_relative "tile_bag.rb"
require_relative "tile_rack.rb"
require_relative "word.rb"
require_relative "tile_group.rb"
require 'spellchecker'
require 'tempfile'

#This is the Babble class it will contain the run method that is the entry point of the applicatiob
class Babble 
	#this is the constructor of the Babble class. It initialized the TileBag, TileRack and Word objects
	def initialize
		@tb = TileBag.new
		@wd = Word.new
		@tr = TileRack.new
		@game_score = 0
	end
	
	#This is the run method, it is the entry point of the application. It will welcome the user to the game
	#set up the tile rack, check the word the user enters and continue until the user quits or runs out of tiles
	def run
		puts "Welcome to Babble, enter " + "\"" + ":quit" + "\"" + " to end the game"
		word = ""
		until ((@tb.empty? && @tr.number_of_tiles_needed == TileRack.class_variable_get(:@@MAX_TILES)) || word == ":quit")
			set_up
			word = gets.chomp
			check_word word
		end
		puts "Thanks for playing, total score: " + @game_score.to_s 
	end

	#These are the private helper methods that will be called in the run method
	private 
	def check_word word
		if word == ""
			puts "You did not enter a word"
		elsif word == ":quit"
			return
		elsif !@tr.has_tiles_for?(word)
			puts "Not enough tiles"
		elsif !Spellchecker::check(word)[0][:correct]
			puts "Not a valid word"
		else
			@wd = @tr.remove_word(word)
			@game_score += @wd.score
			puts "You made " + @wd.hand + " for " + @wd.score.to_s + " points"
		end
	end

	def set_up
		@tr.number_of_tiles_needed.times {@tr.append(@tb.draw_tile)}
		puts ""
		puts "Tile Rack: " + @tr.hand
		puts "Score: " + @game_score.to_s
		puts ""
		puts "Enter word to play"
	end
	
end

Babble.new.run
