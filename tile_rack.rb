require_relative "tile_group.rb"

#This is the TileRack class, it will check how many tiles are needed to replenish the rack and if 
#the rack has the tiles for a word to be played. It will also return the word to be played and 
#remove the corresponding tiles from the rack
class TileRack < TileGroup
	@@MAX_TILES = 7	
	
	def initialize
		super
	end 

	#returns the number of tiles needed to keep the rack at its max length
	def number_of_tiles_needed
		@@MAX_TILES - tiles.length
	end

	#checks to make sure the rack has the correct tiles to play the word provided in the parameter
	def has_tiles_for?(text)
		has_tiles = true
		temp_tiles = tiles.dup
		
		word_array(text).each {|tile| 
				if temp_tiles.include?(tile)
					index_of_tile = temp_tiles.index(tile)
					temp_tiles.delete_at(index_of_tile)
				else
					has_tiles = false
				end
				} 
		has_tiles
	end

	#returns the word that is played and removes the tiles from the rack
	def remove_word text		
		user_word = Word.new
		text.each_char {|char| user_word.tiles.append char.upcase.to_sym
					remove char.upcase.to_sym}
		user_word
	end



	private
	def word_array text
		word_tiles = []
		text.each_char {|char| word_tiles << char.upcase.to_sym }
		
		word_tiles
	end
end
