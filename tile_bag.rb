#Joey Marinelli 1/9/19
#This is the TileBag class. It will contain the methods for the tiles in the babble game
class TileBag
	@@points = {
		:A => 1,
		:B => 3,
		:C => 3,
		:D => 2,
		:E => 1,
		:F => 4,
		:G => 2,
		:H => 4,
		:I => 1,
		:J => 8,
		:K => 5, 
		:L => 1,
		:M => 3,
		:N => 1,
		:O => 1,
		:P => 3,
		:Q => 10,
		:R => 1,
		:S => 1,
		:T => 1, 
		:U => 1,
		:V => 4,
		:W => 4,
		:X => 8,
		:Y => 4,
		:Z => 10	
		}
	attr_accessor :tile_bag
	
	# this is the initialize method. It will accept no parameters and populate the bag of tiles
	def initialize
		@tile_bag = []
		populate_bag
	end

	# this is the getter method for the class variable points
	def self.points 
		return @@points
	end

	#this method will draw a random tile from the tile bag
	def draw_tile
		index = rand(@tile_bag.length)
		tile = @tile_bag[index]
		@tile_bag.delete_at(index)
		return tile
	end
	
	#this method will return true if the bag is empty and false otherwise
	def empty?
		if @tile_bag.length == 0
			return true
		else 
			return false
		end
	end
	
	#this method will return the point value for the given tile
	def self.points_for(tile) 
		return @@points[tile]
	end

	private

	# this method will populate the tile bag with the proper distribution
	def populate_bag
		9.times {@tile_bag << :A}
		2.times {@tile_bag << :B}
		2.times {@tile_bag << :C}
		4.times {@tile_bag << :D}
		12.times {@tile_bag << :E}
		2.times {@tile_bag << :F}
		3.times {@tile_bag << :G}
		2.times {@tile_bag << :H}
		9.times {@tile_bag << :I}
		@tile_bag << :J
		@tile_bag << :K
		4.times {@tile_bag << :L}
		2.times {@tile_bag << :M}
		6.times {@tile_bag << :N}
		8.times {@tile_bag << :O}
		2.times {@tile_bag << :P}
		@tile_bag << :Q
		6.times {@tile_bag << :R}
		4.times {@tile_bag << :S}
		6.times {@tile_bag << :T}
		4.times {@tile_bag << :U}
		2.times {@tile_bag << :V}
		2.times {@tile_bag << :W}
		@tile_bag << :X
		2.times {@tile_bag << :Y}
		@tile_bag << :Z
	end

end
	
